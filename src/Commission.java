public class Commission extends Hourly {
    private Double totalSales;
    private Double commisionRate;

    public Commission(String eName, String eAddress, String ePhone, String socSecNumber, double rate,
            Double commisionRate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        this.commisionRate = commisionRate;
        totalSales = 0.0;
    }

    public void addSales(double totalSales) {
        this.totalSales += totalSales;
    }

    public double pay() {
        double payment = super.pay() + (totalSales * commisionRate);
        totalSales = 0.0;
        return payment;
    }

    public String toString() {
        String result = super.toString();
        result += "\nTotal Sales: " + totalSales;

        return result;
    }
}
